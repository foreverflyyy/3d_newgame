#include <scene.hpp>

namespace nu {
	Scene::Scene(int width, int height) {
		m_width = width;
		m_height = height;
		m_window = std::make_unique<sf::RenderWindow>(sf::VideoMode(m_width, m_height), "CS 2.0");
		m_texture = std::make_unique<sf::Texture>();
		m_texture->create(m_width, m_height);
		m_sprite = std::make_unique<sf::Sprite>(*m_texture);

		Intrinsic intrinsic = { 960.0, 540.0, 960.0, 540.0 };
		Point position = { 0.0, 0, 0.0 };
		Angles angles = { 0.0,0.0,0.0 };
		m_camera = std::make_unique<Camera>(m_width, m_height, intrinsic, position, angles);

        m_object = std::make_unique<Object>(points);
	}
	Scene::~Scene() {
		if (m_points != nullptr)
			delete[] m_points;
	}

    void Scene::CreateAWP(){
        m_texture_2 = std::make_unique<sf::Texture>();

        m_texture_2->loadFromFile("img/awp_top.png", sf::IntRect(0, 0, 320, 180));
        m_sprite_2 = std::make_unique<sf::Sprite>(*m_texture_2);

        m_sprite_2->setPosition(sf::Vector2f(1050, 530));
        m_sprite_2->scale(sf::Vector2f(3.f, 3.f));
    }
    /*void Scene::Shot(){
        shot = true;
        presentShotX = 1080;
        presentShotY = 610;
        DrawShot();

        //float newX = 948;
        //float newY = 520;

        presentShotX += 0.005*time; //������ ��� ����������� �� "������". ���������� ������� �� ���� �������� ������������ ������� � ��������. ������� 0.005 ����� �������� �������� ��������
        if (presentShotX > 3) presentShotX -= 3;
        m_sprite_3->setTextureRect(IntRect(96 * int(presentShotX), 96, 96, 96)); //���������� �� ����������� �. ���������� �������� ��������� � ���������� � ������ 0,96,96*2, � ����� 0
        m_sprite_3->move(-0.1*time, 0);//���������� ���� �������� ��������� �����

        shot = false;

        //m_sprite_3->setPosition(sf::Vector2f(1080, 610));
        //m_sprite_3->setPosition(sf::Vector2f(948, 520));
    }*/

    void Scene::DrawShot(){
        m_texture_3 = std::make_unique<sf::Texture>();

        m_texture_3->loadFromFile("img/shot.png", sf::IntRect(0, 0, 130, 130));
        m_sprite_3 = std::make_unique<sf::Sprite>(*m_texture_3);

        m_sprite_3->setPosition(sf::Vector2f(presentShotX-timer, presentShotY-timer*0.68));
        m_sprite_3->scale(sf::Vector2f(1/2.f, 1/2.f));
    }

    void Scene::Crossing(){}



    void Scene::LifeCycle() {


        m_object->ReadFile();
        //m_object->ReadSmallTerror();
        m_object->ReadTerror();

        //����� � ���� ���
        CreateAWP();

        //�������� ������
        m_window->setMouseCursorVisible(false);

        //��������� �����
        FPS fps;
        sf::Font font;
        if (!font.loadFromFile("fonts/arial.ttf")){
            throw std::runtime_error("ERROR: font was not loaded.");
        }
        sf::Text text_fps;
        text_fps.setFont(font);
        text_fps.setCharacterSize(24);
        text_fps.setPosition(10, 950);
        text_fps.setFillColor(sf::Color::White);

        sf::Text text_score;
        text_score.setFont(font);
        text_score.setCharacterSize(24);
        text_score.setPosition(10, 10);
        text_score.setFillColor(sf::Color::White);

		while (m_window->isOpen()) {
			sf::Event event;
			while (m_window->pollEvent(event)) {
                if (event.type == sf::Event::Closed)
                    m_window->close();
            }

            if(timer>=1){
                timer+=24;
                m_sprite_3->setPosition(sf::Vector2f(presentShotX-timer, presentShotY-timer*0.68));
                m_sprite_2->setPosition(sf::Vector2f(1150-timer, 630-timer*0.68));
            }
            if(timer >= 132){
                timer = 0;
                shot = false;
            }

            if (sf::Keyboard::isKeyPressed(sf::Keyboard::W)){
                m_camera->dZ(0.35);
            }
            if (sf::Keyboard::isKeyPressed(sf::Keyboard::S)){
                m_camera->dZ(-0.35);
            }
            if (sf::Keyboard::isKeyPressed(sf::Keyboard::A)){
                m_camera->dX(-0.35);
            }
            if (sf::Keyboard::isKeyPressed(sf::Keyboard::D)){
                m_camera->dX(0.35);
            }
            if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape)){
                m_window->close();
            }

            sf::Vector2i globalPosition = sf::Mouse::getPosition();

            if (sf::Mouse::isButtonPressed(sf::Mouse::Left)){
                headshot++;
                shot = true;
                DrawShot();
                timer = 1;

                if((1207 < globalPosition.x && globalPosition.x < 1220) && (147 < globalPosition.y && globalPosition.y < 157)){
                    std::cout << "HEADSHOT!" << std::endl;
                    m_object->randomTerror(-9, -23, 0);
                } else if((958 < globalPosition.x && globalPosition.x < 965) && (283 < globalPosition.y && globalPosition.y < 288)){
                    std::cout << "DOUBLE KILL!!!" << std::endl;
                    m_object->randomTerror(15, 51, 0);
                } else if((1752 < globalPosition.x && globalPosition.x < 1761) && (236 < globalPosition.y && globalPosition.y < 241)){
                    std::cout << "MONSTER KILL!!!" << std::endl;
                    m_object->ReadTerror();
                }
                //else if((991 < globalPosition.x && globalPosition.x < 1011) && (174 < globalPosition.y && globalPosition.y < 193)
                //            || (17521 < globalPosition.x && globalPosition.x < 17611) && (236 < globalPosition.y && globalPosition.y < 241)){
                    //std::cout << "MONSTER KILL!!!" << std::endl;
                    //m_object->ReadTerror();
                //}
            }

            if(move < 16 && move > 0) {
                move += 0.5;
                timer_move = 0;
            } else if(move > -16 && move < 0) {
                move -= 0.5;
                timer_move = 0;
            }
            if(move == -16){
                move = 0;
                next = true;
            } else if(move == 16){
                move = 0;
                last = true;
            }
            if(move == 0){
                timer_move++;
            }
            if(timer_move == 15 && last){
                move = -1;
                last = false;
            } else if(timer_move == 15 && next){
                move = 1;
                next = false;
            }
            cout << "timer: " << timer_move << "; move: " << move << endl;

            //std::cout << "X: " << globalPosition.x << "; Y: " << globalPosition.y << std::endl;

            //��� ������
            fps.update();
            std::ostringstream ss;
            ss << fps.getFPS();
            text_fps.setString(ss.str());

            //������� ������
            text_score.setString(std::string("Score: ") + std::to_string(score));

            m_points = m_object->getPoints();
            m_pixels = m_object->getPixels();

            for (int i = 0; i < points; i++) {
                if(i >= 264719){
                    m_points[i].x += move/3;
                    //m_points[i].x = cosa*m_points[i].x + sina*m_points[i].y;
                    m_points[i].y += move;
                    //m_points[i].y = -m_points[i].x*sina+m_points[i].y*cosa;
                    m_camera->ProjectPoint(m_points[i], {m_pixels[i].r, m_pixels[i].g, m_pixels[i].b, 255});
                } else {
                    m_camera->ProjectPoint(m_points[i], {m_pixels[i].r, m_pixels[i].g, m_pixels[i].b, 255});
                }
            }

            m_camera->MouseWork();

            m_texture->update((uint8_t*)m_camera->Picture(), 1920, 1080, 0, 0);
			m_camera->Clear();


			m_window->clear();
			m_window->draw(*m_sprite);
            m_window->draw(*m_sprite_2);
            if(shot) m_window->draw(*m_sprite_3);
            m_window->draw(text_fps);
            m_window->draw(text_score);

			m_window->display();


		}
	}
}

/*
// �������� ������������
for (int i = 0; i < gribs.size(); i++) {
int X = mario->GetX();
int Y = mario->GetY();
float R = mario->GetR();

int x = gribs[i]->GetX();
int y = gribs[i]->GetY();
float r = gribs[i]->GetR();

float d = sqrt((X - x) * (X - x) + (Y - y) * (Y - y));

if (R + r >= d) {
lose = true;
//                    std::cout << "You loser!";
//window.close();
//                delete gribs[i];
//                gribs.erase(gribs.begin() + i);
//                i--;
}
}*/

//����������� ����������� ������
/*template<typename T>
bool are_crossing(vector<T, 2> const &v11, vector<T, 2> const &v12, vector<T, 2> const &v21, vector<T, 2> const &v22, vector<T, 2> *crossing) {
    vector<T, 3> cut1(v12-v11), cut2(v22-v21);
    vector<T, 3> prod1, prod2;

    prod1 = cross(cut1 * (v21-v11));
    prod2 = cross(cut1 * (v22-v11));

    if(sign(prod1[Z]) == sign(prod2[Z]) || (prod1[Z] == 0) || (prod2[Z] == 0)) // �������� ����� � ����������� ������
        return false;

    prod1 = cross(cut2 * (v11-v21));
    prod2 = cross(cut2 * (v12-v21));

    if(sign(prod1[Z]) == sign(prod2[Z]) || (prod1[Z] == 0) || (prod2[Z] == 0)) // �������� ����� � ����������� ������
        return false;

    if(crossing) { // ���������, ���� �� ���������� ����� �����������
        (*crossing)[X] = v11[X] + cut1[X]*fabs(prod1[Z])/fabs(prod2[Z]-prod1[Z]);
        (*crossing)[Y] = v11[Y] + cut1[Y]*fabs(prod1[Z])/fabs(prod2[Z]-prod1[Z]);
    }
    return true;
}*/